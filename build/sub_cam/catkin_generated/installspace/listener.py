#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import Image
import cv2, cv_bridge

class Follower:
    def __init__(self):
        self.image2_pub = rospy.Publisher('python_cam/image_raw', Image)
        self.bridge = cv_bridge.CvBridge()
        #cv2.namedWindow("window", 1)
        self.image_sub = rospy.Subscriber('usb_cam/image_raw', Image, self.image_callback)

    def image_callback(self, msg):
        cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        (rows,cols,channels) = cv_image.shape
        frame = cv2.flip(cv_image, 0)
        #cv2.imshow("Image window", frame)
        cv2.waitKey(3)
        self.image2_pub.publish(self.bridge.cv2_to_imgmsg(frame, "bgr8"))
    
rospy.init_node('follower')
follower = Follower()
rospy.spin()
cv2.destroyAllWindows()